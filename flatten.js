


function myflatten(elements) {
  let toFlat = [];
  function removeArray(item) {
    Array.isArray(item) ? checkItems(item) : toFlat.push(item);
  }

  function checkItems(array) {
    for (let item of array) {
      removeArray(item);
    }
  }

  checkItems(elements);
  return toFlat;
}



module.exports = {
  myflatten,
};
