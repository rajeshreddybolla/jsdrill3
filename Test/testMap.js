let map = require('../map').map;

//const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code. 
//let cb = () => "";
//cb = ( x ) => x*2;

const persons = [
  { firstname: "Malcom", lastname: "Reynolds" },
  { firstname: "Kaylee", lastname: "Frye" },
  { firstname: "Jayne", lastname: "Cobb" }
];

function cb(item) {
  return [item.firstname, item.lastname].join(" ");
}

console.log(map(persons, cb));
