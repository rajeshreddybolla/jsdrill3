

function myfilter(elements, cb) {
  let filtered =[];
  if (!elements || !Array.isArray(elements)) {
    return null;
  }
  for ( let value of elements ){
    cb(value) ? filtered.push(value) : null;
  }
  return filtered;
}

module.exports = {
  myfilter,
};

