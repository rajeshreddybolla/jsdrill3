
function myeach(elements, cb) {
  if(!elements || !cb || elements.length === 0) return;
   for (let i = 0; i < elements.length; i++ ) {
     let value  = elements[i];
    cb( value , i , elements );
  }
}


module.exports = {
  myeach,
};