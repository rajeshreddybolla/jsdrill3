
function map(elements, cb) {
  let returnArray = [];
  if(!elements || !Array.isArray(elements)){
    return [];
  }
  for (let i = 0; i < elements.length;i++){
      let value = elements[i];
    returnArray.push(cb(value , i , elements));
  }
  return returnArray;
}

 exports.map = map;

 module.exports = {
   map,
 };