
function myfind(elements, cb) {
  if (!elements || !Array.isArray(elements)) {
    return null;
  }
  let found = undefined;
  for (let i = 0; i < elements.length; ++i) {
    if (cb(elements[i])) {
      return elements[i]
    }
  }
  return found;
}



module.exports = {
  myfind,
};